<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/login', 'HomeController@showLogin');
Route::post('/login', 'HomeController@doLogin');
Route::any('/registrar', 'HomeController@registrar');
Route::any('/categoria/{slug}', 'CategoryController@index');

Route::group(['middleware' => 'auth'], function () {
  Route::any('/logout', 'HomeController@logout');
  Route::get('/perfil', 'HomeController@perfil');

  Route::any('/ads', 'AdsController@lists');
  Route::any('/add-ads', 'AdsController@add');
  Route::get('/ads-remove/{id}', 'AdsController@remove');
  Route::any('/ads/{id}', 'AdsController@edit');
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', function() {
      return redirect('/admin/login');
    });

    Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::any('/logout', 'AdminAuth\LoginController@logout');

    Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
    Route::post('/register', 'AdminAuth\RegisterController@register');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
