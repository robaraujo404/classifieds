<?php
Route::get('/home', function () {
    return view('admin.home');
})->name('home');




Route::group(['middleware' => 'App\Http\Middleware\RedirectIfNotAdmin'], function () {
  Route::get('/users', 'Admin\UserController@index')->name('users.index');
  Route::get('/users/create', 'Admin\UserController@create')->name('users.create');
  Route::post('/users', 'Admin\UserController@store')->name('users.store');
  Route::put('/users/{users}', 'Admin\UserController@update')->name('users.update');
  Route::patch('/users/{users}', 'Admin\UserController@update')->name('users.update');
  Route::delete('/users/{users}','Admin\UserController@destroy')->name('users.destroy');
  Route::get('/users/{users}', 'Admin\UserController@show')->name('users.show');
  Route::get('/users/{users}/edit', 'Admin\UserController@edit')->name('users.edit');

  Route::get('/ads', ['as'=> 'ads.index', 'uses' => 'Admin\AdsController@index']);
  Route::post('/ads', ['as'=> 'ads.store', 'uses' => 'Admin\AdsController@store']);
  Route::get('/ads/create', ['as'=> 'ads.create', 'uses' => 'Admin\AdsController@create']);
  Route::put('/ads/{ads}', ['as'=> 'ads.update', 'uses' => 'Admin\AdsController@update']);
  Route::patch('/ads/{ads}', ['as'=> 'ads.update', 'uses' => 'Admin\AdsController@update']);
  Route::delete('/ads/{ads}', ['as'=> 'ads.destroy', 'uses' => 'Admin\AdsController@destroy']);
  Route::get('/ads/{ads}', ['as'=> 'ads.show', 'uses' => 'Admin\AdsController@show']);
  Route::get('/ads/{ads}/edit', ['as'=> 'ads.edit', 'uses' => 'Admin\AdsController@edit']);

  Route::get('/categories', ['as'=> 'categories.index', 'uses' => 'Admin\CategoryController@index']);
  Route::post('/categories', ['as'=> 'categories.store', 'uses' => 'Admin\CategoryController@store']);
  Route::get('/categories/create', ['as'=> 'categories.create', 'uses' => 'Admin\CategoryController@create']);
  Route::put('/categories/{categories}', ['as'=> 'categories.update', 'uses' => 'Admin\CategoryController@update']);
  Route::patch('/categories/{categories}', ['as'=> 'categories.update', 'uses' => 'Admin\CategoryController@update']);
  Route::delete('/categories/{categories}', ['as'=> 'categories.destroy', 'uses' => 'Admin\CategoryController@destroy']);
  Route::get('/categories/{categories}', ['as'=> 'categories.show', 'uses' => 'Admin\CategoryController@show']);
  Route::get('/categories/{categories}/edit', ['as'=> 'categories.edit', 'uses' => 'Admin\CategoryController@edit']);
});
