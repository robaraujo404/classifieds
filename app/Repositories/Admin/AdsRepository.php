<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Ads;
use InfyOm\Generator\Common\BaseRepository;

class AdsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'category_id',
        'title',
        'description',
        'price',
        'resume',
        'visits',
        'active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ads::class;
    }
}
