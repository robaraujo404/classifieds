<?php

namespace App\Repositories\Admin;

use App\Models\Admin\User;
use InfyOm\Generator\Common\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'gender',
        'first_name',
        'last_name',
        'about',
        'phone',
        'phone_hidden',
        'email',
        'password',
        'is_admin',
        'ip_addr',
        'active',
        'last_login_at',
        'remaining_ads',
        'purchased_ads',
        'purchased_ads_date',
        'cpf',
        'remember_token',
        'state',
        'city',
        'address'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
