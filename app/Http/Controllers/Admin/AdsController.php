<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateAdsRequest;
use App\Http\Requests\Admin\UpdateAdsRequest;
use App\Repositories\Admin\AdsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AdsController extends AppBaseController
{
    /** @var  AdsRepository */
    private $adsRepository;

    public function __construct(AdsRepository $adsRepo)
    {
        $this->adsRepository = $adsRepo;
    }

    /**
     * Display a listing of the Ads.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->adsRepository->pushCriteria(new RequestCriteria($request));
        $ads = $this->adsRepository
          ->with('user', 'category')
          ->all();

        return view('admin.ads.index')
            ->with('ads', $ads);
    }

    /**
     * Show the form for creating a new Ads.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.ads.create')
          ->with([
            'categories'=> \App\Category::getTreeList(),
            'users'=> \App\User::pluck('first_name', 'id')->toArray()
          ]);
    }

    /**
     * Store a newly created Ads in storage.
     *
     * @param CreateAdsRequest $request
     *
     * @return Response
     */
    public function store(CreateAdsRequest $request)
    {
        $input = $request->all();

        $ads = $this->adsRepository->create($input);

        Flash::success('Ads saved successfully.');

        return redirect(route('admin.ads.index'));
    }

    /**
     * Display the specified Ads.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ads = $this->adsRepository->findWithoutFail($id);

        if (empty($ads)) {
            Flash::error('Ads not found');

            return redirect(route('admin.ads.index'));
        }

        return view('admin.ads.show')->with('ads', $ads);
    }

    /**
     * Show the form for editing the specified Ads.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ads = $this->adsRepository->findWithoutFail($id);

        if (empty($ads)) {
            Flash::error('Ads not found');

            return redirect(route('admin.ads.index'));
        }

        return view('admin.ads.edit')
          ->with('ads', $ads)
          ->with([
            'categories'=> \App\Category::getTreeList(),
            'users'=> \App\User::pluck('first_name', 'id')->toArray(),
          ]);
    }

    /**
     * Update the specified Ads in storage.
     *
     * @param  int              $id
     * @param UpdateAdsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdsRequest $request)
    {
        $ads = $this->adsRepository->findWithoutFail($id);

        if (empty($ads)) {
            Flash::error('Ads not found');

            return redirect(route('admin.ads.index'));
        }

        $ads = $this->adsRepository->update($request->all(), $id);

        Flash::success('Ads updated successfully.');

        return redirect(route('admin.ads.index'));
    }

    /**
     * Remove the specified Ads from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ads = $this->adsRepository->findWithoutFail($id);

        if (empty($ads)) {
            Flash::error('Ads not found');

            return redirect(route('admin.ads.index'));
        }

        $this->adsRepository->delete($id);

        Flash::success('Ads deleted successfully.');

        return redirect(route('admin.ads.index'));
    }
}
