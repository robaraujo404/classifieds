<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;

class HomeController extends \App\Http\Controllers\Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (Auth::guard('admin')->user()) {
        return view('admin.home');
      } else {
        return redirect('/admin/login');
      }
    }
}
