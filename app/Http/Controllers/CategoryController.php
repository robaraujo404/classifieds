<?php

namespace App\Http\Controllers;

use App\Category;
use App\Ads;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index($slug) {
      $cat = Category::where('slug', $slug)->first();
      if (!$cat) {
        return redirect('/');
      }
      $cat_ids = [];
      $category_name = $cat->name;
      if ($cat->parent_id < 1) {
        $cat_ids = Category::where('parent_id', $cat->id)->pluck('id')->toArray();
      }
      $cat_ids[] = $cat->id;
      $ads = Ads::whereIn('category_id', $cat_ids)->get()->toArray();

      return view('home.category', [
    		'categories'    => Category::getTree(),
        'ads_list'      => $ads,
        'category_name' => $category_name
    	]);
    }
}
