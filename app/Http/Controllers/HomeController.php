<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Category;
use App\Ads;
use App\User;
use Validator;
use Input;
use Auth;
use Redirect;

class HomeController extends Controller
{
    public function index()
    {

      // obtem 4 primeira categorias para listar na home
      $ads_by_category = Category::where('parent_id', '<', 1)->limit(4)->get()->toArray();
      foreach ($ads_by_category as $key=>$category) {
        $categories = Category::where('parent_id', $category['id'])->pluck('id')->toArray();
        $ads_by_category[$key]['itens'] = Ads::whereIn('category_id', $categories)->limit(6)->get()->toArray();
      }
    	return view('home.index', [
    		'categories' => Category::getTree(),
        'ads_by_category'=>$ads_by_category
    	]);
    }


	public function showLogin()
	{
    if (Auth::check()) {
      return Redirect::action('HomeController@index');
    }
		return view('home.login', [
    		'categories' => Category::getTree(),
    		'isLogged'=> Auth::check()
    	]);
	}
	public function doLogin()
	{
		$rules = array(
			'email'    => 'required|email',
			'password' => 'required|alphaNum|min:3'
		);

		$request = Input::only('email', 'password');
		$validator = Validator::make($request, $rules);

		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator)
				->withInput(Input::except('password'));
		}

		if (!Auth::attempt($request)) {
			return Redirect::back()->with('loginError', 'Login ou senha incorreta.');
		}

		return Redirect::action('HomeController@index');
	}

	public function logout()
  {
		Auth::logout();
		return Redirect::back();
	}

  public function registrar ()
  {
    $r = Input::all();
    $user_id = Auth::id();

    if (isset($r['first_name'])) {
      $validator = Validator::make($r, [
          'gender' => 'string|required|max:1',
          'first_name' => 'string|required|min:1',
          'last_name' => 'string|required|min:1',
          'about' => 'string',
          'phone' => 'string',
          'cpf' => 'string',
          'email' => 'email|required',
          'password' => 'confirmed|required|min:6',
      ]);
      if ($validator->fails()) {
  			return Redirect::back()->withErrors($validator);
  		}

      $r['active'] = 1;
      $r['is_admin'] = 0;
      $r['password'] = Hash::make($r['password']);
      $user = User::create($r);

      if($user) {
        Auth::loginUsingId($user->id);
        return Redirect::action('HomeController@index')->with('success', 'Cadastro criado com sucesso.');
      }
      return Redirect::back()->with('error', 'Não foi possível criar seu cadastro no momento.');
    }
    return view('home.registrar', [
      'categories' => Category::getTree()
    ]);
  }
  public function perfil() {
    $r = Input::all();
    $user = Auth::user();

    if (isset($r['first_name'])) {
      $validator = Validator::make($r, [
          'gender' => 'string|required|max:1',
          'first_name' => 'string|required|min:1',
          'last_name' => 'string|required|min:1',
          'about' => 'string',
          'phone' => 'string',
          'cpf' => 'string',
          'email' => 'email|required',
          'password' => 'confirmed|required|min:6',
      ]);
      if ($validator->fails()) {
  			return Redirect::back()->withErrors($validator);
  		}

      $r['active'] = 1;
      $r['is_admin'] = 0;
      $r['password'] = Hash::make($r['password']);

      if(User::create($r)) {
        return Redirect::action('HomeController@index')->with('success', 'Cadastro criado com sucesso.');
      }
      return Redirect::back()->with('error', 'Não foi possível criar seu cadastro no momento.');
    }
    return view('home.perfil', [
      'user'=>$user,
      'categories' => Category::getTree()
    ]);
  }
}
