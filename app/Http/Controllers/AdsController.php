<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Ads;
use App\Picture;
use Auth;
use Input;
use Redirect;

class AdsController extends Controller
{
    public function add() {
      $r = Input::all();
      $user_id = Auth::id();

      if (isset($r['title'])) {
        if (!Input::hasFile('pictures')) {
          return Redirect::back()->with('error', 'É necessário ter no mínimo uma imagem.');
        }

        $r['user_id'] = $user_id;
        $ads = Ads::create($r);
        $pictures = [];

        foreach(Input::file('pictures') as $file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath = base_path() . '\public\image\ads';
            $file->move($destinationPath, $picture);

            Picture::create(['filename'=>$picture, 'ad_id'=> $ads->id]);
        }

        if ($ads) {
          return Redirect::action('AdsController@lists')->with('success', 'Ads inserido com sucesso.');
        }
        return Redirect::action('AdsController@lists')->with('error', 'Não foi possível inserir o ads.');
      }

      return view('ads.add', [
        'categories' => Category::getTree(),
        'categories_list' => Category::getTreeList(),
      ]);
    }

    public function lists() {
      return view('ads.list', [
        'categories' => Category::getTree(),
        'ads' => Ads::where('user_id', Auth::id())->get(),
      ]);
    }

    public function edit($id) {
      $r = Input::all();
      $user_id = Auth::id();
      $ads = Ads::find($id);

      if (!$ads || $ads->user_id !== $user_id) {
        return Redirect::back()->with('error', 'Não foi possível acessar este ads.');
      }

      if (isset($r['title'])) {
        if (!Input::hasFile('pictures')) {
          return Redirect::back()->with('error', 'É necessário ter no mínimo uma imagem.');
        }

        $ads->category_id = $r['category_id'];
        $ads->title = $r['title'];
        $ads->description = $r['description'];
        $ads->price = $r['price'];

        $ads->save();
        $pictures = [];

        foreach(Input::file('pictures') as $file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = date('His').$filename;
            $destinationPath = base_path() . '\public\image\ads';
            $file->move($destinationPath, $picture);

            Picture::create(['filename'=>$picture, 'ad_id'=> $ads->id]);
        }

        if ($ads) {
          return Redirect::action('AdsController@lists')->with('success', 'Ads editado com sucesso.');
        }
        return Redirect::action('AdsController@lists')->with('error', 'Não foi possível editar o ads.');
      }

      return view('ads.edit', [
        'ads'=>$ads,
        'categories' => Category::getTree(),
        'categories_list' => Category::getTreeList(),
      ]);

    }

    public function remove($id) {
      $ads = Ads::find($id);
      if (!$ads || $ads->user_id !== Auth::id()) {
        return Redirect::back()->with('error', 'Não foi possível remover este ads.');
      }
      Picture::where('ad_id', $ads->id)->delete();
      $ads->delete();
      return Redirect::back()->with('success', 'Ads removido com sucesso.');
    }
}
