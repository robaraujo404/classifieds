<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	public $table = 'categories';

    protected $fillable = [
        'parent_id','name','slug','description','picture','active'
    ];

    public static function getTree() {
    	$parents = Category::where('parent_id','=', 0)->get();
    	$categories = Category::where('parent_id','!=', 0)->get();
    	$categories_resp = [];

    	foreach ($parents as $category) {
    		$categories_resp[$category->id] = $category->toArray();
    		$categories_resp[$category->id]['children'] = [];
    	}
    	foreach ($categories as $category) {
    		$categories_resp[$category->parent_id]['children'][] = $category;
    	}
    	return $categories_resp;
    }
		public static function getTreeList() {
			$resp = [];
			foreach (self::getTree() as $parent) {
				foreach ($parent['children'] as $child) {
					$p_name = $parent['name'];
					if (!isset($resp[$p_name])) $resp[$p_name] = [];

					$resp[$p_name][$child['id']] = $child['name'];
				}
			}
			return $resp;
		}
}
