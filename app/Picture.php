<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
  public $table = 'pictures';

  protected $fillable = [
      'ad_id', 'filename', 'active'
  ];
}
