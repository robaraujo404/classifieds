<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models\Admin
 * @version February 14, 2017, 9:06 pm UTC
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'gender',
        'first_name',
        'last_name',
        'about',
        'phone',
        'phone_hidden',
        'email',
        'password',
        'is_admin',
        'ip_addr',
        'active',
        'last_login_at',
        'remaining_ads',
        'purchased_ads',
        'purchased_ads_date',
        'cpf',
        'remember_token',
        'state',
        'city',
        'address'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'gender' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'about' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'password' => 'string',
        'ip_addr' => 'string',
        'remaining_ads' => 'integer',
        'purchased_ads' => 'integer',
        'purchased_ads_date' => 'date',
        'cpf' => 'string',
        'remember_token' => 'string',
        'state' => 'string',
        'city' => 'string',
        'address' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
