<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    public $table = 'ads';

    protected $fillable = [
        'user_id', 'category_id', 'title','description','price','resume','visits','active'
    ];


    public function user() {
      return $this->belongsTo(User::class);
    }

    public function category() {
      return $this->belongsTo(Category::class);
    }

    public function pictures() {
      return $this->hasMany(\App\Picture::class);
    }
}
