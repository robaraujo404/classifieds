@extends('layouts.default')
@section('content')
			<ul class="breadcrumb">
				<li><a href="#"><i class="fa fa-home"></i></a></li>
				<li><a>Contas</a></li>
				<li><a href="#">Login</a></li>
			</ul>

			<div class="row">
				<div id="content" class="col-sm-12">
					<div class="page-login">

						<div class="account-border">
							<div class="row">
								{{ Form::open(array('url' => 'login','method'=>'post')) }}
									<div class="col-sm-6 col-sm-offset-3 customer-login">
										<div class="well">
											<h2><i class="fa fa-file-text-o" aria-hidden="true"></i> Acessar minha conta</h2>
											<p><strong>Entre com seus dados</strong></p>

											@if (Session::get('loginError'))
											<div class="alert alert-danger">{{ Session::get('loginError') }}</div>
											@endif
											<p>
												{{ $errors->first('email') }}
												{{ $errors->first('password') }}
											</p>

											<div class="form-group">
												{{ Form::label('email', 'E-mail', array('class'=>'control-label')) }}
												{{ Form::text('email', null, array('seu@email.com', 'class'=>'form-control')) }}
											</div>
											<div class="form-group">
												{{ Form::label('password', 'Senha', array('class'=>'control-label')) }}
												{{ Form::password('password', array('class'=>'form-control')) }}
											</div>
										</div>
										<div class="bottom-form">
											<a href="#" class="forgot">Esqueci minha senha</a>
											<input type="submit" value="Entrar" class="btn btn-default pull-right" />
										</div>
									</div>
								{{ Form::close() }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- //Main Container -->
@stop
