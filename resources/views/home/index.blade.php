@extends('layouts.default')
@section('content')
	<main id="content" class="page-main">
		<!-- Block Spotlight1  -->
		<div class="so-spotlight1 ">
			<div class="container">
				<div class="row">
					<div id="yt_header_right" class="col-lg-offset-3 col-lg-9 col-md-12">
						<div class="slider-container ">
							<div id="so-slideshow" class="">
								<div class="module slideshow no-margin">
									<div class="item">
										<a href="#"><img src="image/demo/slider/sl11/1.jpg" alt="slider1" class="img-responsive"></a>
										<div class="sohomeslider-description">
											<img class="image image-sl11 pos-right img-active" src="image/demo/slider/sl11/2.png" alt="slideshow">
											<div class="text pos-right text-sl11">
												<h3 class="tilte modtitle-sl11  title-active">Nikon D7100</h3>
												<p class="des des-sl11 des-active">Ultimate image quality. Create without limination</p>
											</div>
										</div>
									</div>
									<div class="item">
										<a href="#"><img src="image/demo/slider/sl12/1.jpg" alt="slider1" class="img-responsive"></a>
										<div class="sohomeslider-description">
											<img class="image image-sl12 pos-left img-active" src="image/demo/slider/sl12/2.png" alt="slideshow">
											<div class="text pos-right text-sl12">
											<h3 class="tilte modtitle-sl12 title-active">OUR NEW RANGE OF</h3>
											<h4 class="titleh4 h4-active">TABLET</h4>
											<p class="des des-sl11 des-active">FOR LESS THAN $99.00</p>
											</div>
										</div>
									</div>
									<div class="item">
										<a href="#"><img src="image/demo/slider/sl13/1.jpg" alt="slider1" class="img-responsive"></a>
										<div class="sohomeslider-description">
											<img class="image image-sl13 pos-right img-active" src="image/demo/slider/sl13/2.png" alt="slideshow">
											<div class="text pos-left text-sl13">
												<h3 class="tilte modtitle-sl13 title-active">OUR NEW RANGE OF</h3>
												<h4 class="titleh4 h4-active">IMACS</h4>
											</div>
										</div>
									</div>
								</div>
								<div class="loadeding"></div>

							</div>
						</div>
					</div>
				</div>
				<div class="block-banner banners banner-sn-1 wow fadeInUp">
					<div class="img-1 banner1-1">
						<a title="Static Image" href="#"><img src="image/demo/banner/m1.jpg" alt="Static Image"></a>
					</div>
					<div class="img-1 banner1-2">
						<a title="Static Image" href="#"><img src="image/demo/banner/m2.jpg" alt="Static Image"></a>
					</div>
					<div class="img-1 banner1-3">
						<a title="Static Image" href="#"><img src="image/demo/banner/m3.jpg" alt="Static Image"></a>
					</div>
					<div class="img-1 banner1-4">
						<a title="Static Image" href="#"><img src="image/demo/banner/m4.jpg" alt="Static Image"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="so-spotlight3">
			<div class="container">
				@foreach ($ads_by_category as $category)
					<div class="module cus-style-supper-cate supper2">
						<div class="header">
								<h3 class="modtitle">
								<span class="icon-color">
									<i class="fa fa-mobile"></i>
									{{$category['name']}}
									<small class="arow-after"></small>
								</span>
								<strong class="line-color"></strong>
							</h3>

						</div>
						<div id="so_super_category_1" class="so-sp-cat first-load">
						<div class="spcat-wrap ">
							<div class="spcat-tabs-container" data-delay="300" data-duration="600" data-effect="flip" data-ajaxurl="#" data-modid="155">
								<!--Begin Tabs-->
								    <div class="spcat-tabs-wrap" style="display: none;">
								        <span class="spcat-tab-selected">Sale</span>
								        <span class="spcat-tab-arrow">▼</span>
								        <ul class="spcat-tabs cf">
				                            <li class="spcat-tab  tab-sel tab-loaded" data-active-content=".items-category-sales" data-field_order="sales">
												<span class="spcat-tab-label">Sale</span>
							                </li>
							                <li class="spcat-tab " data-active-content=".items-category-p_sort_order" data-field_order="p_sort_order">
												<span class="spcat-tab-label">Sort Order</span>
							                </li>
							                <li class="spcat-tab" data-active-content=".items-category-rating" data-field_order="rating">
												<span class="spcat-tab-label">Rating</span>
							                </li>
							                <li class="spcat-tab " data-active-content=".items-category-p_quantity" data-field_order="p_quantity">
												<span class="spcat-tab-label">Quantity</span>
							                </li>
							                <li class="spcat-tab " data-active-content=".items-category-p_price" data-field_order="p_price">
												<span class="spcat-tab-label">Price</span>
							                </li>
					                    </ul>
								    </div>
								<!-- End Tabs-->
							</div>

							<div class="main-right">
								<div class="banner-pre-text">

								</div>

								<div class="spcat-items-container products-list grid show-pre show-row"><!--Begin Items-->
									<div class="spcat-items spcat-items-loaded items-category-sales product-layout spcat-items-selected" data-total="9">
										<div class="spcat-items-inner spcat00-4 spcat01-4 spcat02-3 spcat03-2 spcat04-1 flip">
											<div class="ltabs-items-inner ltabs-slider ">
												@foreach ($category['itens'] as $ads)
													<div class="ltabs-item ">
														<div class="item-inner product-thumb product-item-container transition ">
															<div class="left-block">
																<div class="product-image-container">
																	<div class="image">
																			<a class="lt-image" href="product.html" target="_self" title="Verty nolen max..">
																			<img src="image/demo/shop/product/b5.jpg" alt="Apple Cinema 30" title="Verty nolen laben" class="img-1 img-responsive">
																			<img src="image/demo/shop/product/b7.jpg" alt="Apple Cinema 30" title="Verty nolen laben" class="img-2 img-responsive">
																		</a>
																	</div>
																</div>
															</div>
															<div class="right-block">
																<div class="caption">
																	<div class="rating">
																		<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
																			<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
																			<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
																			<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
																			<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
																	</div>
																	<h4>
																		<a href="product.html" title="{{$ads['title']}}" target="_self">
																			 {{ $ads['title'] }}
																		</a>
																	</h4>
																	<p class="price">
																			<span class="price-new">R$ {{$ads['price']}}</span>
																		</p>
																</div>
															</div>
															<div class="button-group">
																<a class="quickview iframe-link visible-lg btn-button" data-toggle="tooltip" title="" data-fancybox-type="iframe" href="quickview.html" data-original-title="Quickview"> <i class="fa fa-search"></i> </a>
															</div>
														</div>
													</div>
												@endforeach
											</div>
										</div>
									</div>
								</div>
								<!--End Items-->
							</div>


						</div>
					</div>
				@endforeach
			</div>
		</div>
	</main >
@stop
