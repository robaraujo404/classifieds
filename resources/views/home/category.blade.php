@extends('layouts.default')
@section('content')
	<main id="content" class="page-main">
		<!-- Block Spotlight1  -->
		<div class="so-spotlight3">
			<div class="container">
					<div class="module cus-style-supper-cate supper2">
						<div class="header">
								<h3 class="modtitle">
								<span class="icon-color">
									<i class="fa fa-mobile"></i>
									{{$category_name}}
									<small class="arow-after"></small>
								</span>
								<strong class="line-color"></strong>
							</h3>

						</div>
						<div id="so_super_category_1" class="so-sp-cat first-load">
						<div class="spcat-wrap ">
							<div class="spcat-tabs-container" data-delay="300" data-duration="600" data-effect="flip" data-ajaxurl="#" data-modid="155">
								<!--Begin Tabs-->
								    <div class="spcat-tabs-wrap" style="display: none;">
								        <span class="spcat-tab-selected">Sale</span>
								        <span class="spcat-tab-arrow">▼</span>
								        <ul class="spcat-tabs cf">
				                            <li class="spcat-tab  tab-sel tab-loaded" data-active-content=".items-category-sales" data-field_order="sales">
												<span class="spcat-tab-label">Sale</span>
							                </li>
							                <li class="spcat-tab " data-active-content=".items-category-p_sort_order" data-field_order="p_sort_order">
												<span class="spcat-tab-label">Sort Order</span>
							                </li>
							                <li class="spcat-tab" data-active-content=".items-category-rating" data-field_order="rating">
												<span class="spcat-tab-label">Rating</span>
							                </li>
							                <li class="spcat-tab " data-active-content=".items-category-p_quantity" data-field_order="p_quantity">
												<span class="spcat-tab-label">Quantity</span>
							                </li>
							                <li class="spcat-tab " data-active-content=".items-category-p_price" data-field_order="p_price">
												<span class="spcat-tab-label">Price</span>
							                </li>
					                    </ul>
								    </div>
								<!-- End Tabs-->
							</div>

							<div class="main-right">
								<div class="banner-pre-text">

								</div>

								<div class="spcat-items-container products-list grid show-pre show-row"><!--Begin Items-->
									<div class="spcat-items spcat-items-loaded items-category-sales product-layout spcat-items-selected" data-total="9">
										<div class="spcat-items-inner spcat00-4 spcat01-4 spcat02-3 spcat03-2 spcat04-1 flip">
											<div class="ltabs-items-inner ltabs-slider ">
												@foreach ($ads_list as $ads)
													<div class="ltabs-item ">
														<div class="item-inner product-thumb product-item-container transition ">
															<div class="left-block">
																<div class="product-image-container">
																	<div class="image">
																			<a class="lt-image" href="product.html" target="_self" title="Verty nolen max..">
																			<img src="image/demo/shop/product/b5.jpg" alt="Apple Cinema 30" title="Verty nolen laben" class="img-1 img-responsive">
																			<img src="image/demo/shop/product/b7.jpg" alt="Apple Cinema 30" title="Verty nolen laben" class="img-2 img-responsive">
																		</a>
																	</div>
																</div>
															</div>
															<div class="right-block">
																<div class="caption">
																	<div class="rating">
																		<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
																			<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
																			<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
																			<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
																			<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
																	</div>
																	<h4>
																		<a href="product.html" title="{{$ads['title']}}" target="_self">
																			 {{ $ads['title'] }}
																		</a>
																	</h4>
																	<p class="price">
																			<span class="price-new">R$ {{$ads['price']}}</span>
																		</p>
																</div>
															</div>
															<div class="button-group">
																<a class="quickview iframe-link visible-lg btn-button" data-toggle="tooltip" title="" data-fancybox-type="iframe" href="quickview.html" data-original-title="Quickview"> <i class="fa fa-search"></i> </a>
															</div>
														</div>
													</div>
												@endforeach
											</div>
										</div>
									</div>
								</div>
								<!--End Items-->
							</div>


						</div>
					</div>
			</div>
		</div>
	</main >
@stop
