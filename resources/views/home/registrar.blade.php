@extends('layouts.default')
@section('content')
  <div class="row content1">
			<div id="content" class="col-sm-12">
				<h2 class="title">Criar uma nova conta</h2>
				<p>Se você já possui uma conta conosco, por favor acesse a <a href="{{URL::to('/')}}/login">sessão de login</a>.</p>
        {{ Form::open(array('files' => true, 'class'=>"form-horizontal account-register clearfix")) }}
          <fieldset id="account">
						<legend>Seus Dados Pessoais</legend>
						<div class="form-group required" style="display: none;">
							<label class="col-sm-2 control-label">Customer Group</label>
							<div class="col-sm-10">
								<div class="radio">
									<label>
										<input type="radio" name="customer_group_id" value="1" checked="checked"> Default
									</label>
								</div>
							</div>
						</div>
            <div class="form-group required">
              {{ $errors->first('first_name') }}
              {{ Form::label('first_name', 'Primeiro Nome', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::text('first_name', null, array('class'=>'form-control')) }}
              </div>
            </div>
            <div class="form-group required">
              {{ $errors->first('last_name') }}
              {{ Form::label('last_name', 'Ultimo Nome', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::text('last_name', null, array('class'=>'form-control')) }}
              </div>
            </div>
            <div class="form-group required">
              {{ $errors->first('gender') }}
              {{ Form::label('gender', 'Gênero', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::select('gender', [null=>'-- Escolha --','m'=>'Homem','f'=>'Mulher'], null, array('class'=>'form-control')) }}
              </div>
            </div>
            <div class="form-group required">
              {{ $errors->first('email') }}
              {{ Form::label('email', 'E-mail', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::email('email', null, array('placeholder'=>'E-mail', 'class'=>'form-control')) }}
              </div>
            </div>
            <div class="form-group required">
              {{ $errors->first('cpf') }}
              {{ Form::label('cpf', 'CPF', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::text('cpf', null, array('placeholder'=>'CPF','class'=>'form-control')) }}
              </div>
            </div>
            <div class="form-group required">
              {{ $errors->first('phone') }}
              {{ Form::label('phone', 'Telefone', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::text('phone', null, array('placeholder'=>'Telefone','class'=>'form-control')) }}
              </div>
            </div>
					</fieldset>
					<fieldset id="address">
						<legend>Endereço</legend>

            <div class="form-group required">
              {{ $errors->first('address') }}
              {{ Form::label('address', 'Endereço', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::text('address', null, array('placeholder'=>'Endereço','class'=>'form-control')) }}
              </div>
            </div>
            <div class="form-group required">
              {{ $errors->first('state') }}
              {{ Form::label('state', 'Estado', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::text('state', null, array('placeholder'=>'Estado','class'=>'form-control')) }}
              </div>
            </div>
            <div class="form-group required">
              {{ $errors->first('city') }}
              {{ Form::label('city', 'Cidade', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::text('city', null, array('placeholder'=>'Cidade','class'=>'form-control')) }}
              </div>
            </div>
					</fieldset>
					<fieldset>
						<legend>Sua Senha</legend>
            <div class="form-group required">
              {{ $errors->first('password') }}
              {{ Form::label('password', 'Senha', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::password('password', array('placeholder'=>'Senha','class'=>'form-control')) }}
              </div>
            </div>
            <div class="form-group required">
              {{ $errors->first('password_confirmation') }}
              {{ Form::label('password_confirmation', 'Confirme a Senha', array('class'=>'control-label col-sm-2')) }}
              <div class="col-sm-10">
                {{ Form::password('password_confirmation', array('placeholder'=>'Repetir Senha','class'=>'form-control')) }}
              </div>
            </div>
					</fieldset>
					<div class="buttons">
						<div class="pull-right">Eu li e concordo com os <a href="#" class="agree"><b>Termos de uso</b></a>
							<input class="box-checkbox" type="checkbox" name="agree" value="1"> &nbsp;
							<input type="submit" value="Continue" class="btn btn-primary">
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
@stop
