	<footer class="footer-container typefooter-1">
			<!-- FOOTER TOP -->
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="module social_block col-md-1 col-sm-12 col-xs-12" >
								<ul class="social-block ">
									<li class="facebook"><a class="_blank" href="https://www.facebook.com/mercado.mfindustrial/?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a></li>
								</ul>
							</div>
							<div class="module news-letter col-md-9 col-sm-12 col-xs-12">
								<div class="newsletter">
									<div class="title-block">
										<div class="page-heading">ASSINE NOSSA NEWSLETTER</div>
										<div class="pre-text">
											Para receber incríveis anúncios!
										</div>
									</div>
									<div class="block_content">
										<form method="post" name="signup" id="signup" class="btn-group form-inline signup">
											<div class="form-group required send-mail">
												<div class="input-box">
													<input type="email" placeholder="Seu endereço de email" value="" class="form-control" id="txtemail" name="txtemail" size="55">
												</div>
												<div class="subcribe">
													<button class="btn btn-default btn-lg" type="submit" onclick="return subscribe_newsletter();" name="submit">
														<Inscrever-me></Inscrever-me>						</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- FOOTER CENTER -->
			<div class="footer-center">
				<div class="container content">
					<div class="row">
						<!-- BOX INFO -->
						<div class="col-md-3 col-sm-6 col-xs-12 collapsed-block footer-links box-footer">
							<div class="module ">
								<div class="content-block-footer">
									<div class="footer-logo">
										<a href="index.html"><img src="{{URL::to('/image/logo1.png')}}" title="Your Store" alt="Your Store" /></a>
									</div>
									<p style="text-align: center;">
										Seja bem vindo ao Mercado Industrial, o maior portal de negócios da indústria brasileira.<br>
										Aqui você anuncia e vende!<br>

										
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 box-account">
						</div>
						<div class="col-md-3 col-sm-6 box-account box-footer">
						</div>
						<!-- BOX ABOUT -->
						<div class="col-md-3  col-sm-6 collapsed-block box-footer">
							<div class="module ">
								<h3 class="modtitle">Sobre Nós</h3>
								<div class="modcontent">
									<ul class="contact-address">
										<li><span class="fa fa-home"></span> Centro - Marília - SP </li>
										<li><span class="fa fa-envelope"></span> Email: <a href="#"> contato@mfindustrial.com.br</a></li>
										<li><span class="fa fa-phone">&nbsp;</span> Telefone: 14 99845 4725</li>
									</ul>
									<ul class="payment-method" style="display:none;">
										<li><a title="Payment Method" href="#"><img src="image/demo/cms/payment/payment-1.png" alt="Payment"></a></li>
										<li><a title="Payment Method" href="#"><img src="image/demo/cms/payment/payment-2.png" alt="Payment"></a></li>
										<li><a title="Payment Method" href="#"><img src="image/demo/cms/payment/payment-3.png" alt="Payment"></a></li>
										<li><a title="Payment Method" href="#"><img src="image/demo/cms/payment/payment-4.png" alt="Payment"></a></li>
										<li><a title="Payment Method" href="#"><img src="image/demo/cms/payment/payment-5.png" alt="Payment"></a></li>
									</ul>
									<div class="payment-method">
										<img src="{{URL::to('/image/bandeiras-pagseguro.png')}}">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- FOOTER BOTTOM -->
			<div class="footer-bottom ">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
						Mercado Industrial © 2017. All Rights Reserved.
						</div>
						<div class="back-to-top"><i class="fa fa-angle-up"></i><span> Top </span></div>
					</div>
				</div>
			</div>

		</footer>
