<!-- Header Container  -->
		<header id="header" class=" variantleft type_1 header-home1">
			<!-- Header Top -->
			<div class="header-top compact-hidden">
				<div class="container">
					<div class="row">
						<div class="header-top-left  col-lg-4  hidden-sm col-md-5 hidden-xs">
							<ul class="list-inlines">
								<li class="hidden-xs" >
									<div class="welcome-msg">
									</div>
								</li>
							</ul>

						</div>
						<div class="header-top-right collapsed-block col-lg-8 col-sm-12 col-md-7 col-xs-12 ">
							<h5 class="tabBlockTitle visible-xs">More<a class="expander " href="#TabBlock-1"><i class="fa fa-angle-down"></i></a></h5>
							<div class="tabBlock" id="TabBlock-1">
								<ul class="top-link list-inline">
									<li class="account" id="my_account">
										<a href="#" title="My Account" class="btn btn-xs dropdown-toggle" data-toggle="dropdown"> <span >Minha Conta</span> <span class="fa fa-angle-down"></span></a>
										<ul class="dropdown-menu ">
											@if (Auth::check())
												<li><a href="{{URL::to('/')}}/perfil"><i class="fa fa-pencil-square-o"></i> Perfil</a></li>
												<li><a href="{{URL::to('/')}}/ads"><i class="fa fa-user"></i> Meus Anuncios</a></li>
												<li><a href="{{URL::to('/')}}/logout"><i class="fa fa-user"></i> Sair</a></li>
											@else
												<li><a href="{{URL::to('/')}}/registrar"><i class="fa fa-user"></i> Registrar</a></li>
												<li><a href="{{URL::to('/')}}/login"><i class="fa fa-pencil-square-o"></i> Login</a></li>
											@endif
										</ul>
									</li>
									<li class="checkout hidden"><a href="checkout.html" class="top-link-checkout" title="Checkout"><span >Checkout</span></a></li>
									<li class="login hidden"><a href="cart.html" title="Shopping Cart"><span >Shopping Cart</span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //Header Top -->
			<!-- Header center -->
			<div class="header-center">
				<div class="container">
					<div class="row">
						<!-- LOGO -->
						<div class="navbar-logo col-md-3 col-sm-4 col-xs-10">
						   <a href="{{URL::to('/')}}"><img src="{{URL::to('/image/logo2.png')}}" title="Your Store" alt="Your Store" /></a>
						</div>
						<div class="header-center-right col-md-9 col-sm-8 col-xs-2">
							<div class="responsive so-megamenu  megamenu-style-dev">
								<nav class="navbar-default">
									<div class=" container-megamenu  horizontal">
										<div class="navbar-header">
											<button type="button" id="show-megamenu" data-toggle="collapse" class="navbar-toggle">
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
											</button>
										</div>

										<div class="megamenu-wrapper ">
											<span id="remove-megamenu" class="fa fa-times"></span>
											<div class="megamenu-pattern">
												<div class="container">
													<ul class="megamenu " data-transition="slide" data-animationtime="250">
														<li class="home hover">
															<a href="index.html">Home <b class="caret"></b></a>
														</li>
														@foreach ($categories as $category)
															<li class="with-sub-menu hover">
																<p class="close-menu"></p>
																<a href="{{URL::to('/categoria', $category['slug'])}}" class="clearfix">
																	<strong>
																		{{$category['name']}}
																	</strong>
																	<span class="label"></span>
																	<b class="caret"></b>
																</a>
																<div class="sub-menu" style="width: 100%; display: none;">
																	<div class="content">
																		<div class="row">
																			@foreach ($category['children'] as $child)
																			<div class="col-md-3">
																			    <a href="{{URL::to('/categoria', $child['slug'])}}">
																			    	{{$child['name']}}
																			    </a>
																			</div>
																			@endforeach
																		</div>
																	</div>
																</div>
															</li>
														@endforeach
													</ul>
												</div>
											</div>
										</div>
									</div>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //Header center -->
			<!-- Header Bottom -->
			<div class="header-bottom compact-hidden">
				<div class="container">
					<div class="header-bottom-inner">
						<div class="row">
							<div class="header-bottom-left menu-vertical col-md-3 col-sm-2 col-xs-2">
								<div class="responsive so-megamenu megamenu-style-dev">
									<div class="so-vertical-menu no-gutter compact-hidden">
										<nav class="navbar-default">
											<div class="container-megamenu vertical open">
												<div id="menuHeading">
													<div class="megamenuToogle-wrapper">
														<div class="megamenuToogle-pattern">
															<div class="container">
																<div>
																	<span></span>
																	<span></span>
																	<span></span>
																</div>
																Categorias
																<i class="fa pull-right arrow-circle fa-chevron-circle-up"></i>
															</div>
														</div>
													</div>
												</div>
												<div class="navbar-header">
													<button type="button" id="show-verticalmenu" data-toggle="collapse" class="navbar-toggle">
														<span class="icon-bar" style="width: 12px;"></span>
														<span class="icon-bar" style="width: 16px;"></span>
														<span class="icon-bar"></span>
													</button>
												</div>
												<div class="vertical-wrapper" >
													<span id="remove-verticalmenu" class="fa fa-times"></span>
													<div class="megamenu-pattern">
														<div class="container">
															<ul class="megamenu">
																@foreach ($categories as $category)
																	<li class="item-vertical style1 with-sub-menu hover">
																		<p class="close-menu"></p>
																		<a href="#" class="clearfix">
																			<strong>
																				<span>{{$category['name']}}</span>
																				<b class="fa fa-angle-right"></b>
																			</strong>
																		</a>
																		<div class="sub-menu" data-subwidth="100" >
																			<div class="content" >
																				<div class="row">
																					<div class="col-sm-8">
																						<div class="categories ">
																							<div class="row">
																								<div class="col-sm-6 static-menu">
																									<div class="menu">
																										<ul>
																											@foreach ($category['children'] as $child)
																												<li>
																													<a>
																														{{$child['name']}}
																													</a>

																												</li>
																											@endforeach
																										</ul>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</li>
																@endforeach
															</ul>
														</div>
													</div>
												</div>
											</div>
										</nav>
									</div>
								</div>
							</div>

							<!-- Main menu -->
							<div class="header-bottom-right col-md-9 col-sm-10 col-xs-10">
								<div class="col-lg-9 col-md-8 col-sm-7 col-xs-9 header_search">
									<!-- Search -->
									<div id="sosearchpro" class="search-pro">
										<form method="GET" action="index.html">
											<div id="search0" class="search input-group">
												<div class="select_category filter_type  icon-select">
													<select class="no-border" name="category_id">
														<option value="0">Todas Categorias</option>
														@foreach ($categories as $category)
														<option value="">{{$category['name']}}</option>
														    @foreach ($category['children'] as $child)
														    	<option value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$child['name']}}</option>
														    @endforeach
														@endforeach
													</select>
												</div>

												<input class="autosearch-input form-control" type="text" value="" size="50" autocomplete="off" placeholder="Search" name="search">
												<span class="input-group-btn">
												<button type="submit" class="button-search btn btn-primary" name="submit_search"><i class="fa fa-search"></i></button>
												</span>
											</div>
											<input type="hidden" name="route" value="product/search" />
										</form>
									</div>
									<!-- //end Search -->
								</div>
								<div class="header_custom_link hidden-xs">
									<ul>
										@if (!Auth::check())
										<li><a href="{{URL::to('/')}}/login"><i class="fa fa-user"></i> Login</a></li>
										@endif
									</ul>
								</div>
							</div>
							<!-- //end Main menu -->
						</div>
					</div>
				</div>
			</div>
		</header>
