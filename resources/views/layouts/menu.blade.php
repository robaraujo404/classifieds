<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('admin.users.index') !!}"><i class="fa fa-edit"></i><span>Usuários</span></a>
</li>
<li class="{{ Request::is('ads*') ? 'active' : '' }}">
    <a href="{!! route('admin.ads.index') !!}"><i class="fa fa-edit"></i><span>Anúncios</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('admin.categories.index') !!}"><i class="fa fa-edit"></i><span>Categorias</span></a>
</li>

