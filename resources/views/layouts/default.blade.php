
<!DOCTYPE html>
<html lang="pt-BR">
<head>

    <!-- Basic page needs
	============================================ -->
	<title>Classificados</title>
	<meta charset="utf-8">
    <meta name="keywords" content="boostrap, responsive, html5, css3, jquery, theme, multicolor, parallax, retina, business" />
    <meta name="author" content="Magentech">
    <meta name="robots" content="index, follow" />

	<!-- Mobile specific metas
	============================================ -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Favicon
	============================================ -->
    <link rel="shortcut icon" href="ico/favicon.png">

	<!-- Google web fonts
	============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700|Roboto:400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Libs CSS
	============================================ -->
    <link rel="stylesheet" href="{{URL::to('/')}}/css/bootstrap/css/bootstrap.min.css">
	<link href="{{URL::to('/')}}/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="{{URL::to('/')}}/js/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/js/owl-carousel/owl.carousel.css" rel="stylesheet">
	<link href="{{URL::to('/')}}/css/themecss/lib.css" rel="stylesheet">
	<link href="{{URL::to('/')}}/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">

	<!-- Theme CSS
	============================================ -->
   	<link href="{{URL::to('/')}}/css/themecss/so_megamenu.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/css/themecss/so-categories.css" rel="stylesheet">
	<link href="{{URL::to('/')}}/css/themecss/so-listing-tabs.css" rel="stylesheet">
	<link href="{{URL::to('/')}}/css/themecss/animate.css" rel="stylesheet">
	<link href="{{URL::to('/')}}/css/themecss/so-super-category.css" rel="stylesheet">
	<link id="color_scheme" href="{{URL::to('/')}}/css/theme.css" rel="stylesheet">
	<link id="color_scheme" href="{{URL::to('/')}}/css/home5.css" rel="stylesheet">
	<link href="{{URL::to('/')}}/css/responsive.css" rel="stylesheet">


</head>

<body class="common-home res layout-home1">
    <div id="wrapper" class="wrapper-full banners-effect-7">
		@include('layouts.header')
		<!-- Main Container  -->
		<main id="content" class="page-main">
	    @if (Session::get('error'))
	      <div class="alert alert-danger">{{ Session::get('error') }}</div>
	    @endif
			@if (Session::get('success'))
				<div class="alert alert-success">{{ Session::get('success') }}</div>
			@endif

			@yield('content')
		</main >
		<!-- //Main Container -->

		<script type="text/javascript"><!--
			var $typeheader = 'header-home1';
			//-->
		</script>
		<!-- Footer Container -->
		@include('layouts.footer')
		<!-- //end Footer Container -->
    </div>

	<link rel='stylesheet' property='stylesheet'  href='css/themecss/cpanel.css' type='text/css' media='all' />

	<!-- Preloading Screen -->
	<div id="loader-wrapper">
		<div id="loader"></div>
		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>
	 </div>
	<!-- End Preloading Screen -->

	<!-- Include Libs & Plugins
	============================================ -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="{{URL::to('/')}}/js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/owl-carousel/owl.carousel.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/themejs/libs.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/unveil/jquery.unveil.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/countdown/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/dcjqaccordion/jquery.dcjqaccordion.2.8.min.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/datetimepicker/moment.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/jquery-ui/jquery-ui.min.js"></script>


	<!-- Theme files
	============================================ -->
	<script type="text/javascript" src="{{URL::to('/')}}/js/themejs/application.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/themejs/toppanel.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/themejs/so_megamenu.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/themejs/addtocart.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/js/themejs/cpanel.js"></script>
</body>
</html>
