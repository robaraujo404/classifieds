<!-- Id Field -->


<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Sexo:') !!}
    <p>{!! $user->gender !!}</p>
</div>

<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'Primeiro Nome:') !!}
    <p>{!! $user->first_name !!}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Segundo Nome:') !!}
    <p>{!! $user->last_name !!}</p>
</div>

<!-- About Field -->
<div class="form-group">
    {!! Form::label('about', 'Sobre:') !!}
    <p>{!! $user->about !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Telefone') !!}
    <p>{!! $user->phone !!}</p>
</div>

<!-- Phone Hidden Field -->


<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'E-mail:') !!}
    <p>{!! $user->email !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Senha:') !!}
    <p>{!! $user->password !!}</p>
</div>

<!-- Is Admin Field -->

<!-- Ip Addr Field -->

<!-- Active Field -->

<!-- Last Login At Field -->

<!-- Remaining Ads Field -->
<div class="form-group">
    {!! Form::label('remaining_ads', 'Anúncios Restantes:') !!}
    <p>{!! $user->remaining_ads !!}</p>
</div>

<!-- Purchased Ads Field -->
<div class="form-group">
    {!! Form::label('purchased_ads', 'Anúncios Comprados:') !!}
    <p>{!! $user->purchased_ads !!}</p>
</div>

<!-- Purchased Ads Date Field -->
<div class="form-group">
    {!! Form::label('purchased_ads_date', 'Data de Compra dos Anúncios') !!}
    <p>{!! $user->purchased_ads_date !!}</p>
</div>

<!-- Cpf Field -->
<div class="form-group">
    {!! Form::label('cpf', 'CPF:') !!}
    <p>{!! $user->cpf !!}</p>
</div>

<!-- Remember Token Field -->
<div class="form-group">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    <p>{!! $user->remember_token !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Entrou Em:') !!}
    <p>{!! $user->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizado Em:') !!}
    <p>{!! $user->updated_at !!}</p>
</div>

<!-- State Field -->
<div class="form-group">
    {!! Form::label('state', 'Estado:') !!}
    <p>{!! $user->state !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'Cidade:') !!}
    <p>{!! $user->city !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Endereço:') !!}
    <p>{!! $user->address !!}</p>
</div>

