<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Gênero:') !!}
    {!! Form::select('gender', [null=>'-- Selecione --', 'm'=>'Masculino', 'f'=>'Feminino'], null, ['class' => 'form-control']) !!}
</div>

<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'Primeiro Nome:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Segundo Nome:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>

<!-- About Field -->
<div class="form-group col-sm-6">
    {!! Form::label('about', 'Sobre:') !!}
    {!! Form::text('about', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Telefpne:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'E-mail:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Senha:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Ip Addr Field -->


<!-- Last Login At Field -->

<!-- Remaining Ads Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remaining_ads', 'Anúncios Restantes') !!}
    {!! Form::number('remaining_ads', null, ['class' => 'form-control']) !!}
</div>

<!-- Purchased Ads Field -->
<div class="form-group col-sm-6">
    {!! Form::label('purchased_ads', 'Anúncios Comprados:') !!}
    {!! Form::number('purchased_ads', null, ['class' => 'form-control']) !!}
</div>

<!-- Purchased Ads Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('purchased_ads_date', 'Data de compra de anúncios:') !!}
    {!! Form::date('purchased_ads_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Cpf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpf', 'CPF:') !!}
    {!! Form::text('cpf', null, ['class' => 'form-control']) !!}
</div>

<!-- Remember Token Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    {!! Form::text('remember_token', null, ['class' => 'form-control']) !!}
</div>

<!-- State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state', 'Estado:') !!}
    {!! Form::text('state', null, ['class' => 'form-control']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'Cidade:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Endereço:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.users.index') !!}" class="btn btn-default">Cancelar</a>
</div>
