@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Anúncio
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ads, ['route' => ['admin.ads.update', $ads->id], 'method' => 'patch']) !!}

                        @include('admin.ads.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection