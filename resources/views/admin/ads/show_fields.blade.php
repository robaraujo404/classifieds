<!-- Id Field -->


<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'Usuário:') !!}
    <p>{!! $ads->user_id !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Categoria:') !!}
    <p>{!! $ads->category_id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Título:') !!}
    <p>{!! $ads->title !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Descrição:') !!}
    <p>{!! $ads->description !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Preço:') !!}
    <p>{!! $ads->price !!}</p>
</div>

<!-- Resume Field -->


<!-- Visits Field -->

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Ativo:') !!}
    <p>{!! $ads->active !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado Em:') !!}
    <p>{!! $ads->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizado Em:') !!}
    <p>{!! $ads->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Excluído Em:') !!}
    <p>{!! $ads->deleted_at !!}</p>
</div>

