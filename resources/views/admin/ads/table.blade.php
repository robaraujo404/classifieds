<table class="table table-responsive" id="ads-table">
    <thead>
        <th>Título</th>
        <th>Usuário</th>
        <th>Categoria</th>
        <th>Preço</th>
        <th>Visitas</th>
        <th>Ativo</th>
        <th colspan="3">Ação</th>
    </thead>
    <tbody>
    @foreach($ads as $ads)
        <tr>
            <td>{!! $ads->title !!}</td>
            <td>{!! $ads->user->first_name.' '. $ads->user->last_name !!}</td>
            <td>{!! $ads->category->name !!}</td>
            <td>{!! $ads->price !!}</td>
            <td>{!! $ads->visits !!}</td>
            <td>{!! $ads->active !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.ads.destroy', $ads->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.ads.show', [$ads->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.ads.edit', [$ads->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
