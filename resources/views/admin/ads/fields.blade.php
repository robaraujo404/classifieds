<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'Usuário:') !!}
    {!! Form::select('user_id', [null=>'-- Selecione --']+$users, null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Categoria') !!}
    {!! Form::select('category_id', [null=>'-- Selecione --']+$categories, null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Título:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Descrição:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Preço:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Resume Field -->

<!-- Visits Field -->

<!-- Images Field -->
<div class="form-group col-sm-6">
    <h4>Imagens</h4>
    @foreach ($ads->pictures as $pic)
      <img src="{{URL::to('/image/ads/'.$pic->filename)}}">
    @endforeach
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.ads.index') !!}" class="btn btn-default">Cancelar</a>
</div>
