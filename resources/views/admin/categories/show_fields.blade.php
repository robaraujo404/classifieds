<!-- Id Field -->

<!-- Parent Id Field -->
<div class="form-group">
    {!! Form::label('parent_id', 'Categoria Pai:') !!}
    <p>{!! $category->parent_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nome:') !!}
    <p>{!! $category->name !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $category->slug !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Descrição:') !!}
    <p>{!! $category->description !!}</p>
</div>

<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('picture', 'Picture:') !!}
    <p>{!! $category->picture !!}</p>
</div>

<!-- Active Field -->


