@extends('layouts.default')
@section('content')
<div class="row content1">
	<!--Middle Part Start-->
	<div class="col-sm-9" id="content">
		<h2 class="title">Novo Anúncio</h2>
		{{ Form::open(array('url' => 'add-ads','files' => true)) }}
			<div class="col-sm-6 customer-login">
				<div class="form-group required">
					{{ $errors->first('title') }}
					{{ Form::label('title', 'Nome', array('class'=>'control-label')) }}
					{{ Form::text('title', null, array('class'=>'form-control')) }}
				</div>
				<div class="form-group required">
					{{ $errors->first('price') }}
					{{ Form::label('price', 'Preço', array('class'=>'control-label')) }}
					{{ Form::text('price', null, array('class'=>'form-control')) }}
				</div>
				<div class="form-group required">
					{{ $errors->first('description') }}
					{{ Form::label('description', 'Descrição', array('class'=>'control-label')) }}
					{{ Form::text('description', null, array('class'=>'form-control')) }}
				</div>
				<div class="form-group required">
					{{ $errors->first('category_id') }}
					{{ Form::label('category_id', 'Categoria', array('class'=>'control-label')) }}
					{{ Form::select('category_id', [null=>'-- Escolha --'] + $categories_list, null, array('class'=>'form-control')) }}
				</div>
				<div class="form-group required">
					{{ $errors->first('pictures') }}
					{{ Form::label('pictures', 'Imagens do anûncio', array('class'=>'control-label')) }}
					{{Form::file('pictures[]', array('class'=>'form-control', 'multiple'=>true))}}
				</div>
				<div class="bottom-form">
					<p>{{ Form::submit('Criar Anuncio', array('class'=>'btn btn-md btn-primary')) }}</p>
				</div>
			</div>
		{{ Form::close() }}
	</div>
	<!--Middle Part End-->
	<!--Right Part Start -->

	<!--Right Part End -->
</div>
@stop
