@extends('layouts.default')
@section('content')
<div class="row content1">
	<!--Middle Part Start-->
	<div class="col-sm-9" id="content">
		<h2 class="title">Novo Anúncio</h2>
    {{ Form::model($ads, array('files' => true)) }}
			<div class="col-sm-6 customer-login">
				<div class="form-group required">
					{{ $errors->first('title') }}
					{{ Form::label('title', 'Nome', array('class'=>'control-label')) }}
					{{ Form::text('title', $ads->title, array('class'=>'form-control')) }}
				</div>
				<div class="form-group required">
					{{ $errors->first('price') }}
					{{ Form::label('price', 'Preço', array('class'=>'control-label')) }}
					{{ Form::text('price', $ads->price, array('class'=>'form-control')) }}
				</div>
				<div class="form-group required">
					{{ $errors->first('description') }}
					{{ Form::label('description', 'Descrição', array('class'=>'control-label')) }}
					{{ Form::text('description', $ads->description, array('class'=>'form-control')) }}
				</div>
				<div class="form-group required">
					{{ $errors->first('category_id') }}
					{{ Form::label('category_id', 'Categoria', array('class'=>'control-label')) }}
					{{ Form::select('category_id', [null=>'-- Escolha --'] + $categories_list, $ads->category_id, array('class'=>'form-control')) }}
				</div>
				<div class="form-group required">
					{{ $errors->first('pictures') }}
					{{ Form::label('pictures', 'Imagens do anûncio', array('class'=>'control-label')) }}
					{{Form::file('pictures[]', array('class'=>'form-control', 'multiple'=>true))}}

          @foreach ($ads->pictures as $pic)
            <div class="ads-pic">
              <img src="{{URL::to('/images/ads/'.$pic->filename)}}">
            </div>
          @endforeach
				</div>
				<div class="bottom-form">
					<p>{{ Form::submit('Alterar Anuncio', array('class'=>'btn btn-md btn-primary')) }}</p>
					<a href="{{URL::to('/ads')}}" class="btn btn-link">Cancelar</a>
				</div>
			</div>
		{{ Form::close() }}
	</div>
	<!--Middle Part End-->
	<!--Right Part Start -->

	<!--Right Part End -->
</div>
@stop
