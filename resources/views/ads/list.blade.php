@extends('layouts.default')
@section('content')
<div class="row content1">
  <!--Middle Part Start-->
  <div class="col-sm-9" id="content">
    <h2 class="title">Seus Ads</h2>
    <div class="pull-right">
      @if (Auth::user()->remaining_ads > 0)
        Você ainda tem <b>{{Auth::user()->remaining_ads}}</b> ads restantes.
        <a href="{{URL::to('/add-ads')}}" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i> Novo</a>
      @else
        Você não possui saldo para postar ads, compre mais pelo link
        <a href="{{URL::to('/comprar-saldo')}}" class="btn btn-link"><i class="glyphicon glyphicon-usd"></i> comprar</a>
      @endif
    </div>
    <div class="clearfix"></div>
    @if (count($ads))
    <table id="container-ads" class="table">
      <thead>
        <tr>
          <td>Título</td>
          <td>Categoria</td>
          <td>Editar</td>
          <td>Excluir</td>
        </tr>
      </thead>
      @foreach ($ads as $ad)
        <tr>
          <td>{{$ad->title}}</td>
          <td>{{$ad->category->name}}</td>
          <td>
            <a href="{{URL::to('/ads/'.$ad->id)}}">
              <i class="glyphicon glyphicon-pencil"></i>
          </td>
          <td>
            <a href="{{URL::to('/ads-remove/'.$ad->id)}}">
              <i class="glyphicon glyphicon-remove-circle"></i>
          </td>
        </tr>
      @endforeach
    </table>
    @else
      Você não postou nenhum ads ainda
    @endif
  </div>
</div>
@stop
