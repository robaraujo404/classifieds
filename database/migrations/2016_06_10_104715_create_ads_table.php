<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->index('user_id');
            $table->integer('category_id')->unsigned()->index('category_id');
            $table->string('title')->default('')->index('title');
            $table->text('description', 65535);
            $table->float('price', 10, 0)->nullable();
            $table->string('resume', 200)->nullable();
            $table->integer('visits')->unsigned()->nullable()->default(0);
            $table->boolean('active')->nullable()->default(0)->index('active');
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads');
    }
}
