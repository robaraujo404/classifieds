<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gender', 1)->nullable();
            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('about')->nullable();
            $table->string('phone', 60)->nullable();
            $table->boolean('phone_hidden')->nullable()->default(0);
            $table->string('email')->unique();
            $table->string('password', 60)->nullable();
            $table->string('ip_addr', 50)->nullable();
            $table->boolean('active')->nullable()->default(1);
            $table->dateTime('last_login_at')->nullable();
            $table->integer('remaining_ads')->defaul(0);
            $table->integer('purchased_ads')->defaul(0);
            $table->date('purchased_ads_date')->nullable();
            $table->string('cpf', 20)->nullable();
            $table->string('address', 120)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('city', 50)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
