<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->delete();
        DB::table('users')->insert([
            [
                'id' => 1,
                'gender'=>'m',
                'first_name' => 'Roberto',
                'last_name' => 'Araujo',
                'about' => 'About me',
                'phone'=>'5199394334',
                'phone_hidden'=>false,
                'email' => 'robaraujo404@gmail.com',
                'password' => Hash::make('123456'),
                'active'=>true,
                'remaining_ads'=>3,
                'purchased_ads'=>10,
                'purchased_ads_date'=> Carbon::now()->format('Y-m-d H:i:s'),
            ],[
                'id' => 2,
                'gender'=>'f',
                'first_name' => 'Flavia',
                'last_name' => 'Silva',
                'about' => '...',
                'phone'=>'5199993433',
                'phone_hidden'=>false,
                'email' => 'junior_s.araujo@gmail.com',
                'password' => Hash::make('123456'),
                'active'=>true,
                'remaining_ads'=>3,
                'purchased_ads'=>9,
                'purchased_ads_date'=> Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ]);
    }
}
