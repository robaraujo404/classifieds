<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->delete();

        \DB::table('categories')->insert(array(
            0   => array(
                'id'               => '1',
                'parent_id'        => '0',
                'name'             => 'Automoveis',
                'slug'             => 'automoveis',
                'description'      => 'Categoria de automovies em geral',
                'picture'          => 'fa-car.png',
                'active'           => '1',
            ),
            1   => array(
                'id'               => '2',
                'parent_id'        => '1',
                'name'             => 'Fiat',
                'slug'             => 'fiat',
                'description'      => 'Categoria de automovies fiat',
                'picture'          => 'fa-car.png',
                'active'           => '1',
            ),
            2   => array(
                'id'               => '3',
                'parent_id'        => '0',
                'name'             => 'Eletronicos',
                'slug'             => 'eletronicos',
                'description'      => 'Categoria de eletronicos em geral',
                'picture'          => 'fa-eletronic.png',
                'active'           => '1',
            ),
            3   => array(
                'id'               => '4',
                'parent_id'        => '3',
                'name'             => 'Celulares',
                'slug'             => 'celulares',
                'description'      => 'Categoria de Celulares',
                'picture'          => 'fa-celular.png',
                'active'           => '1',
            ),
        ));
    }
}
