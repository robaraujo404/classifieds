<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('admins')->delete();
      DB::table('admins')->insert([
          [
              'id' => 1,
              'name' => 'Roberto Araujo',
              'email' => 'robaraujo404@gmail.com',
              'password' => Hash::make('123456'),
          ],[
            'id' => 2,
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('123456'),
          ]
      ]);
    }
}
